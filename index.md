---
layout: page
title: Vinay Keshava
meta-title: About me
---

<div id="aboutme-section">

<p class="about-text">
<!--<span class="fa fa-briefcase about-icon"></span> -->
<br>
<h3>"Hello! I'm  Vinay".</h3>
<br>
</p>

<p class="about-text">
<!-- <span class="fa fa-graduation-cap about-icon"></span> -->
This is my space on the internet.<br>I write posts about the things i would like to share.
<br>
I use GNU/Linux as my primary operating system,I try to do some cool stuff and would like to blog my experiments!.<br>
</p>
<p class="about-text">
<!-- <span class="fa fa-envelope about-icon"></span> -->
I am pursuing Computer Science and Engineering at <a href="https://aiet.org.in/">Alva's Institute of Engineering and Technology</a><br> 
Let’s connect if you want to collaborate and create something awesome in the world of technology.<br>
<a href="https://vinay-keshava.github.io/blog/">Click here </a>to read my Blog.<br>
You can connect over a email <a target="_blank" href="mailto:vinaykeshava@disroot.org">here</a>.<br>
You can also ping me on <a href="https://matrix.to/#/@vinay-keshava:matrix.org">Matrix</a>.<br>
 

<br>
Thank you for visiting.

 
<h3>Latest Posts</h3>
1. <a href="https://vinay-keshava.github.io/2021-12-10-FSCI-Update/">Experience FSCI Server Update</a>.
<!-- <p class="about-text">
<!-- <span class="fa fa-heart about-icon"></span> -->
<!-- I am an avid open source enthusiast, contributor, and passionate about AI as a whole. I love listening to music, cooking and spreading my knowledge to the community. 
Eager in meeting new people, to connect, discuss, network and grow, mostly at academic conferences, dev-fests, and meet-ups.
</p> -->

