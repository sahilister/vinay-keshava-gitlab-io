---
layout: post
title: Experience FSCI Server Update 2021
subtitle: Server Update
tags: ['FSCI','Debian']
---

Ahh, my first blog post.   Good Beginnings  (:   
Excited!!

This blog post is dedicated to sharing my experience with updating the FSCI’s server update. Servers are used to provide services that keep constantly running without any disruptions.

After joining  [**Software Freedom Camp 2021 (Diversity Edition)**](https://camp.fsci.in)   as a learner and I met a bunch of amazing people who discuss free software and educate people about why it is important to use free software in our life maintaining freedom and privacy in this era of technology.

It was a good experience updating the server which I had never done before, since I was a beginner, I had never updated any server.

Initially, Sahil explained how a server works and how to update the server remotely, since I knew ssh and command line, so was able to understand it.
After updating my keys on the server i could update the server remotely.

All thanks to  [Ravish](https://ravish0007.github.io) and [Sahilister](https://blog.sahilister.in)  for helping me.
